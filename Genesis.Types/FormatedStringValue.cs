﻿using System;

namespace Genesis.Types
{
    [Serializable]
    public abstract class FormatedStringValue
    {
        private string _value;
        public string Value { 
            get
            {
                return _value;
            }
            private set
            {
                if (CheckValue(value))
                    _value = NormalizeValue(value);
            }
        }

        protected abstract bool CheckValue(string value);

        protected virtual string NormalizeValue(string value)
        {
            return value;
        }

        protected FormatedStringValue(string value)
        {
            Value = value;
        }

        public override string ToString()
        {
            return Value;
        }    

        public static implicit operator string(FormatedStringValue value)
        {
            return (value != null) ? value.ToString() : null;
        }
    }
}
