using System;

namespace Genesis.Types.Exceptions
{
    public class ParseException : Exception
    {
        public ParseException(string message) : base(message)
        {
        }
    }
}
