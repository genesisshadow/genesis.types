using System;

namespace Genesis.Types.Exceptions
{
    public class IncompatibleConversionException : Exception
    {
        public IncompatibleConversionException(string srcUnit, string dstUnit)
            : base(string.Format(Properties.Resources.IncompatibleConversionExceptionMessage, srcUnit, dstUnit))
        {
        }
    }
}
