using System;

namespace Genesis.Types.Exceptions
{
    public class IncompatibleUnitsException : Exception
    {
        public IncompatibleUnitsException(string srcUnit, string dstUnit)
            : base(string.Format(Properties.Resources.IncompatibleUnitExceptionMessage, srcUnit, dstUnit))
        {
            
        }
    }
}
