﻿using System;
using System.Text.RegularExpressions;

namespace Genesis.Types
{
    [Serializable]
    public class EmailAddress : RegexControlledStringValue
    {
        private static readonly Regex RxEMail;
        static EmailAddress()
        {
            RxEMail = new Regex(@"^([\w\.\-_]{1,}){1,}\@([\w\.\-_]{1,}){1,}([a-zA-Z]{2,7})$", RegexOptions.IgnoreCase | RegexOptions.CultureInvariant | RegexOptions.Compiled | RegexOptions.Singleline);
        }

        public EmailAddress(string value) : base(value)
        {
        }

        protected override Regex GetRegularExpression()
        {
            return RxEMail;
        }

        protected override string NormalizeValue(string value)
        {
            return (value != null) ? value.ToLowerInvariant() : null;
        }

        public static implicit operator EmailAddress(string value)
        {
            return new EmailAddress(value);
        }

        public static bool TryParse(string s, out EmailAddress emailAddress)
        {
            if (!RxEMail.IsMatch(s))
            {
                emailAddress = null;
                return false;
            }
            emailAddress = new EmailAddress(s);
            return true;
        }
    }
}
