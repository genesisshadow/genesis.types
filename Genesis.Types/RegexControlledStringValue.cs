﻿using System;

namespace Genesis.Types
{
    [Serializable]
    public abstract class RegexControlledStringValue : FormatedStringValue
    {
        protected System.Text.RegularExpressions.Regex RegularExpression {
            get { return GetRegularExpression(); }
        }

        protected RegexControlledStringValue(string value) : base(value)
        {
        }

        protected abstract System.Text.RegularExpressions.Regex GetRegularExpression();

        protected override bool CheckValue(string value)
        {
            return (value != null) && (RegularExpression.IsMatch(value));
        }        
    }
}