﻿namespace Genesis.Types
{
    public partial class Money
    {
        public static partial class Consts
        {
            public const string CZK = "CZK";
            public const string EUR = "EUR";
            public const string USD = "USD";
        }
    }
}
