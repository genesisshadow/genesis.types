﻿using System;
using System.Collections.Generic;
using Genesis.Types.Exceptions;

namespace Genesis.Types
{
    public static class Convert
    {
        private static readonly Dictionary<string, decimal> ConversionRatios = new Dictionary<string, decimal>();

        public static void SetConversionRatio(string srcUnit, string dstUnit, decimal? ratio)
        {
            string key = GetConversionRatioKey(srcUnit, dstUnit);
            if (!ratio.HasValue)
                ConversionRatios.Remove(key);
            else
                ConversionRatios[key] = ratio.Value;         
        }

        public static decimal? GetConversionRatio(string srcUnit, string dstUnit)
        {
            string key = GetConversionRatioKey(srcUnit, dstUnit);
            return (ConversionRatios.ContainsKey(key))
                ? (decimal?) ConversionRatios[key] 
                : null;
        }

        private static string GetConversionRatioKey(string srcUnit, string dstUnit)
        {
            return string.Format("{0}>{1}", srcUnit, dstUnit);
        }

        public static Amount To(this Amount amount, string unit)
        {
            if (amount == null)
                return null;
            if (string.IsNullOrWhiteSpace(unit))
                throw new ArgumentNullException(unit);
            decimal? ratio = (string.Equals(amount.Unit, unit)) ? 1M : GetConversionRatio(amount.Unit, unit);
            if (!ratio.HasValue)
                throw new IncompatibleConversionException(amount.Unit, unit);
            return new Amount(amount.Value * ratio.Value, unit);
        }

        public static Money To(this Money money, string unit)
        {
            if (money == null)
                return null;
            if (string.IsNullOrWhiteSpace(unit))
                throw new ArgumentNullException(unit);
            decimal? ratio = (string.Equals(money.Currency, unit)) ? 1M : GetConversionRatio(money.Currency, unit);
            if (!ratio.HasValue)
                throw new IncompatibleConversionException(money.Currency, unit);
            return new Money(money.Value * ratio.Value, unit);
        }        
    }
}
