﻿namespace Genesis.Types
{
    public interface IHTMLGenerator
    {
        string ToHTML();
    }
}
