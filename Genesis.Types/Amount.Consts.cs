﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Genesis.Types
{
    public partial class Amount
    {
        public static partial class Consts 
        {
            public const string mm = "mm";
            public const string cm = "cm";
            public const string dm = "dm";
            public const string m = "m";
            public const string km = "km";

            public const string pcs = "pcs";
            public const string l = "l";
            public const string hl = "hl";
            public const string ml = "m;";
        }
    }
}
