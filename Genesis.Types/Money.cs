﻿#region

using System;
using System.Globalization;
using System.Text.RegularExpressions;
using System.Threading;
using Genesis.Types.Exceptions;
using Genesis.Types.Properties;

#endregion

namespace Genesis.Types
{
    [Serializable]
    public partial class Money : Amount
    {
        public string Currency
        {
            get { return base.Unit; }
        }

        // ReSharper disable UnusedMember.Local   
        private new string Unit
        {
            get { return base.Unit; }
        }

        // ReSharper restore UnusedMember.Local

        public Money(decimal value, string currency) : base(value, currency)
        {
        }

        public override string ToString()
        {
            return ToString("{0:C}");
        }

        public override string ToString(string format)
        {
            return string.Format(CreateFormatProvider(Currency), format, Value);
        }

        private static IFormatProvider CreateFormatProvider(string currency, NumberFormatInfo nfi = null)
        {
            if (nfi == null)
                nfi = Thread.CurrentThread.CurrentCulture.NumberFormat.Clone() as NumberFormatInfo;
            if (nfi != null)
                nfi.CurrencySymbol = currency;
            return nfi;
        }

        private static readonly Regex RxCurrencyFinder = new Regex("\\s*[a-zA-Z]{1,3}\\s*", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static bool TryParse(string s, out Money output)
        {
            if (TryParse(s, null, out output))
                return true;
            if (TryParse(s, CultureInfo.InvariantCulture.NumberFormat.Clone() as NumberFormatInfo, out output))
                return true;
            return false;
        }


        public static bool TryParse(string s, NumberFormatInfo nfi, out Money output)
        {
            string currency = null;

            Match match = RxCurrencyFinder.Match(s);
            if (match.Success)
                currency = match.Value.Trim();

            if (string.IsNullOrWhiteSpace(currency))
                throw new ParseException(Resources.ParseExceptionUnknownCurrencyMessage);

            decimal val;

            bool result = decimal.TryParse(s, NumberStyles.Currency, CreateFormatProvider(currency, nfi), out val);
            output = (result) ? new Money(val, currency) : null;

            return result;
        }

        public static Money operator +(Money left, Money right)
        {
            if ((left == null) && (right == null))
                return null;
            if (left == null)
                return new Money(right.Value, right.Unit);
            if (right == null)
                return new Money(left.Value, left.Unit);

            if (right.Unit != left.Unit)
                right = right.To(left.Unit);

            return new Money(left.Value + right.Value, left.Unit);
        }

        public static Money operator -(Money left, Money right)
        {
            if ((left == null) && (right == null))
                return null;
            if (left == null)
                return new Money(right.Value, right.Unit);
            if (right == null)
                return new Money(left.Value, left.Unit);

            if (right.Unit != left.Unit)
                right = right.To(left.Unit);

            return new Money(left.Value - right.Value, left.Unit);
        }

        public static Money operator *(Money left, decimal operand)
        {
            if (left == null)
                return null;

            return new Money(left.Value*operand, left.Unit);
        }

        public static Money operator /(Money left, decimal operand)
        {
            if (left == null)
                return null;

            return new Money(left.Value/operand, left.Unit);
        }

        public static implicit operator Money(string value)
        {
            Money result;
            if (!TryParse(value, out result))
                return null;
            return result;
        }

        public static implicit operator string(Money value)
        {
            return (value != null) ? value.ToString() : null;
        }
    }
}