﻿using System;
using System.Text.RegularExpressions;
using Genesis.Types.Exceptions;

namespace Genesis.Types
{
    [Serializable]
    public partial class Amount : IConvertibleUnit
    {
        public string Unit { get; set; }
        public decimal Value { get; set; }

        public Amount(decimal value, string unit)
        {
            if (unit==null)
                throw new ArgumentNullException("unit");
            if (string.IsNullOrWhiteSpace(unit))
                throw new ArgumentException(string.Format(Properties.Resources.InvalidParameterValueExceptionMessage, "unit", unit));
            Unit = unit;
            Value = value;
        }

        public override string ToString()
        {
            return ToString("{0}");
        }

        public virtual string ToString(string format)
        {
            return string.Format( format + Unit, Value);
        }
        
        private static readonly Regex RxCurrencyFinder = new Regex("\\s*[a-zA-Z]{1}\\w*\\s*", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static bool TryParse(string s, out Amount output)
        {
            string unit;

            Match match = RxCurrencyFinder.Match(s);
            if (match.Success)
            {
                unit = match.Value.Trim();
                s = RxCurrencyFinder.Replace(s, string.Empty);
            }
            else
                throw new ParseException(Properties.Resources.ParseExceptionUnknownUnitMessage);

            decimal val;

            bool result = decimal.TryParse(s, out val);
            output = (result) ? new Amount(val, unit) : null;

            return result;
        }

        public static Amount operator +(Amount left, Amount right)
        {
            if ((left == null) && (right == null))
                return null;
            if (left == null)
                return new Amount(right.Value, right.Unit);
            if (right == null)
                return new Amount(left.Value, left.Unit);

            if (right.Unit != left.Unit)
                right = right.To(left.Unit);

            return new Amount(left.Value + right.Value, left.Unit);
        }

        public static Amount operator -(Amount left, Amount right)
        {
            if ((left == null) && (right == null))
                return null;
            if (left == null)
                return new Amount(right.Value, right.Unit);
            if (right == null)
                return new Amount(left.Value, left.Unit);

            if (right.Unit != left.Unit)
                right = right.To(left.Unit);

            return new Amount(left.Value - right.Value, left.Unit);
        }

        public static Amount operator *(Amount left, decimal operand)
        {
            if (left == null)
                return null;

            return new Amount(left.Value*operand, left.Unit);
        }

        public static Amount operator /(Amount left, decimal operand)
        {
            if (left == null)
                return null;

            return new Amount(left.Value / operand, left.Unit);
        }

        public static implicit operator Amount(string value)
        {
            Amount result;
            if (!TryParse(value, out result))
                return null;
            return result;

        }

        public static implicit operator string(Amount value)
        {
            return (value != null) ? value.ToString() : null;
        }
    }

    
}
