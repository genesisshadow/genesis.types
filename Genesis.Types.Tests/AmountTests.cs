﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Genesis.Types.Tests
{
    [TestClass]
    public class AmountTests
    {
        [TestMethod]
        public void ToStringTest()
        {
            Amount money = new Amount(10.01M, "mm");

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Assert.AreEqual("10.01mm", money.ToString());
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            Assert.AreEqual("10,01mm", money.ToString());            
        }

        [TestMethod]
        public void TryParseTest()
        {
            Amount amount;
     
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            string input = "10,00mm";
            Assert.IsTrue(Amount.TryParse(input, out amount));
            Assert.AreEqual("mm", amount.Unit);
            Assert.AreEqual(10.00M, amount.Value);

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            input = "10.00dm";
            Assert.IsTrue(Amount.TryParse(input, out amount));
            Assert.AreEqual("dm", amount.Unit);
            Assert.AreEqual(10.00M, amount.Value);

        }

        [TestMethod]
        public void ConversionTest()
        {
            Amount amount;

            Convert.SetConversionRatio("mm", "cm", 0.1M);
            Convert.SetConversionRatio("cm", "mm", 10.0M);
            Convert.SetConversionRatio("cm", "dm", 0.1M);
            Convert.SetConversionRatio("dm", "cm", 10.0M);
            Convert.SetConversionRatio("mm2", "cm2", 0.01M);
            Convert.SetConversionRatio("cm2", "mm2", 100M);

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            string input = "10,00mm";
            Assert.IsTrue(Amount.TryParse(input, out amount));
            Assert.AreEqual("mm", amount.Unit);
            Assert.AreEqual(10.00M, amount.Value);

            Amount converted = amount.To("mm");
            Assert.AreEqual("mm", converted.Unit);
            Assert.AreEqual(10.00M, converted.Value);
            
            converted = amount.To("cm");
            Assert.AreEqual("cm", converted.Unit);
            Assert.AreEqual(1.00M, converted.Value);

            converted = converted.To("mm");
            Assert.AreEqual("mm", converted.Unit);
            Assert.AreEqual(10.00M, converted.Value);

            input = "100,00mm2";
            Assert.IsTrue(Amount.TryParse(input, out amount));
            Assert.AreEqual("mm2", amount.Unit);
            Assert.AreEqual(100.00M, amount.Value);

            converted = amount.To("cm2");
            Assert.AreEqual("cm2", converted.Unit);
            Assert.AreEqual(1.00M, converted.Value);
        }

        [TestMethod]
        public void OperatorTest()
        {
            Amount amount;

            Convert.SetConversionRatio("mm", "cm", 0.1M);
            Convert.SetConversionRatio("cm", "mm", 10.0M);

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            string input = "10,00mm";
            Assert.IsTrue(Amount.TryParse(input, out amount));
            Assert.AreEqual("mm", amount.Unit);
            Assert.AreEqual(10.00M, amount.Value);

            Amount result = amount * 2;
            Assert.AreEqual("mm", result.Unit);
            Assert.AreEqual(20.00M, result.Value);

            result = amount / 2;
            Assert.AreEqual("mm", result.Unit);
            Assert.AreEqual(5.00M, result.Value);

            result = amount + new Amount(5, "mm");
            Assert.AreEqual("mm", result.Unit);
            Assert.AreEqual(15.00M, result.Value);

            result = amount - new Amount(5, "mm");
            Assert.AreEqual("mm", result.Unit);
            Assert.AreEqual(5.00M, result.Value);

            result = amount + new Amount(2, "cm");
            Assert.AreEqual("mm", result.Unit);
            Assert.AreEqual(30.00M, result.Value);

        }
    }
}
