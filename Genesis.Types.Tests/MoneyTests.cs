﻿using System.Globalization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Genesis.Types.Tests
{
    [TestClass]
    public class MoneyTests
    {
        [TestMethod]
        public void ToStringTest()
        {
            Money money = new Money(10, "CZK");

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;
            Assert.AreEqual("CZK10.00", money.ToString());
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            Assert.AreEqual("10,00 CZK", money.ToString("{0:C}"));            
        }

        [TestMethod]
        public void TryParseTest()
        {
            Money money;

            System.Threading.Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;            
            string input = "CZK10.00";
            Assert.IsTrue(Money.TryParse(input, out money));
            Assert.AreEqual("CZK", money.Currency);
            Assert.AreEqual(10.00M, money.Value);

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            input = "10,00 CZK";
            Assert.IsTrue(Money.TryParse(input, out money));
            Assert.AreEqual("CZK", money.Currency);
            Assert.AreEqual(10.00M, money.Value);

            input = "CZK10.00";
            Assert.IsTrue(Money.TryParse(input, out money));
            Assert.AreEqual("CZK", money.Currency);
            Assert.AreEqual(10.00M, money.Value);

        }

        [TestMethod]
        public void ConversionTest()
        {
            Money money;

            Convert.SetConversionRatio("CZK", "EUR", 0.1M);
            Convert.SetConversionRatio("EUR", "CZK", 10.0M);
            Convert.SetConversionRatio("CZK", "USD", 0.1M);
            Convert.SetConversionRatio("USD", "CZK", 10.0M);

            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            string input = "10,00 CZK";
            Assert.IsTrue(Money.TryParse(input, out money));
            Assert.AreEqual("CZK", money.Unit);
            Assert.AreEqual(10.00M, money.Value);

            Amount converted = money.To("CZK");
            Assert.AreEqual("CZK", converted.Unit);
            Assert.AreEqual(10.00M, converted.Value);

            converted = money.To("EUR");
            Assert.AreEqual("EUR", converted.Unit);
            Assert.AreEqual(1.00M, converted.Value);

            converted = converted.To("CZK");
            Assert.AreEqual("CZK", converted.Unit);
            Assert.AreEqual(10.00M, converted.Value);
        }

        [TestMethod]
        public void OperatorTest()
        {
            Money money;

            Convert.SetConversionRatio("CZK", "EUR", 0.1M);
            Convert.SetConversionRatio("EUR", "CZK", 10.0M);
         
            System.Threading.Thread.CurrentThread.CurrentCulture = new CultureInfo("cs-CZ");
            string input = "10,00 CZK";
            Assert.IsTrue(Money.TryParse(input, out money));
            Assert.AreEqual("CZK", money.Unit);
            Assert.AreEqual(10.00M, money.Value);

            Money result = money * 2;
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(20.00M, result.Value);

            result = money / 2;
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(5.00M, result.Value);

            result = money + new Money(5,"CZK");
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(15.00M, result.Value);

            result = money - new Money(5, "CZK");
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(5.00M, result.Value);

            result = money + new Money(2, "EUR");
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(30.00M, result.Value);

            string str = money;
            Assert.AreEqual("10,00 CZK", str);

            result = "50,00 CZK";
            Assert.AreEqual("CZK", result.Unit);
            Assert.AreEqual(50.00M, result.Value);

        }
    }
}
