﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Genesis.Types.Tests
{
    [TestClass]
    public class EmailAddressTests
    {
        [TestMethod]
        public void FormatTest()
        {
            EmailAddress emailAddress;

            foreach (var item in new List<string>
                {
                    "asdf@asdf.aa",
                    "basic@format.aa",
                    "extended.dotted@format.aa",
                    "Extended.CaseInsensitive@Format.AA",
                    "Multi.Dotted.Extended.Case.Insensitive@Format.AA",
                    "Numb3r@format.aa",
                    "321@format.aa",
                    "321@format2.aa",
                    "aaa@main.muzeum",
                    "aaa@second.gov",
                    "aaa@third.people",
                    "a-aa@third.people",
                    "aa_a@third.people"
                })
            {
                Assert.IsTrue(EmailAddress.TryParse(item, out emailAddress), "Format failed by " + item);
            }

            foreach (var item in new List<string>
                {
                    "^@aa.aa",
                    "%&^@aa.aa",
                    "aaa@&(*&.aa"
                })
            {
                Assert.IsFalse(EmailAddress.TryParse(item, out emailAddress), "Format failed by " + item);
            }
            
        }

        [TestMethod]
        public void OperatorTest()
        {
            foreach (var item in new List<string>
                {
                    "test@test.aa"
                })
            {
                EmailAddress emailAddress = item;
                string s = emailAddress;
                Assert.AreEqual(item, s);
            }
        }
    }
}
